<?php
use yii\helpers\Html; 
?>
<div>
    <?= $model->titulo ?>
</div>

<div>
    <?= $model->contenido ?>
</div>
<div>
    <?=    Html::a('Eliminar',
            ['site/eliminar','id' => $model->id],
            ['class' => 'btn btn-danger',
            // confirmacion realizada con javascript y una ventana emergente    
            /**'data' => [
                'confirm' => '¿Seguro que desea eliminarlo?',
                'method' => 'post',
                ],**/
            ],
            )
        ?>
</div>
